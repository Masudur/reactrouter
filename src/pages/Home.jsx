import React from 'react'
import Layout from '../Layouts/Layout'

const Home = () => {
  return (
    <React.Fragment>
        <Layout>
            Home page
        </Layout>
    </React.Fragment>
  )
}

export default Home