import React from 'react'
import { Outlet } from 'react-router-dom'
import Layout from '../Layouts/Layout'

const NutritionProgramsPage = () => {
  return (
    <React.Fragment>
          <Layout >
            NutritionProgramsPage
            <Outlet />
        </Layout>
    </React.Fragment>
  )
}

export default NutritionProgramsPage