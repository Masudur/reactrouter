import React from 'react'
import Layout from '../Layouts/Layout'

const NotFoundPage = () => {
  return (
    <div>
        <Layout>
        NotFoundPage
        </Layout>
    </div>
  )
}

export default NotFoundPage