import React from 'react'
import { Outlet } from 'react-router-dom'
import Layout from '../Layouts/Layout'

const ReportsPage = () => {
  return (
    <React.Fragment>
        <Layout>
            ReportsPage
            <Outlet />
        </Layout>
    </React.Fragment>
  )
}

export default ReportsPage