import React from 'react'
import { Outlet } from 'react-router-dom'
import Layout from '../Layouts/Layout'

const LoginPage = () => {
  return (
    <React.Fragment>
        <Layout>
            LoginPage
            <Outlet />
        </Layout>
    </React.Fragment>
  )
}

export default LoginPage