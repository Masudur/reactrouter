import React from 'react'
import { Outlet } from 'react-router-dom'
import Layout from '../Layouts/Layout'

const ClassRoomPage = () => {
  return (
    <React.Fragment>
        <Layout>
            ClassRoomPage
            <Outlet />
        </Layout>
    </React.Fragment>
  )
}

export default ClassRoomPage