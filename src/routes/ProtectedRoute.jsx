import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import Home from '../pages/Home'
const useAuth = ()=>{
    const user = {isLogin : false}
    return user && user.isLogin
}
const ProtectedRoute = () => {
    const auth = useAuth();

  return auth ? <Outlet/> : <Navigate to='/'/>
}

export default ProtectedRoute