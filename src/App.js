

import { Routes, Route, Link } from "react-router-dom";
import ClassListPage from "./pages/ClassListPage";
import ClassRoomPage from "./pages/ClassRoomPage";
import Home from './pages/Home';
import LoginPage from "./pages/LoginPage";
import NotFoundPage from "./pages/NotFoundPage";
import NutritionBmiPage from "./pages/NutritionBmiPage";
import NutritionDewormingPage from "./pages/NutritionDewormingPage";
import NutritionEducationPage from "./pages/NutritionEducationPage";
import NutritionProgramsPage from "./pages/NutritionProgramsPage";
import NutritionReferralPage from "./pages/NutritionReferralPage";
import NutritionTabletStockPage from "./pages/NutritionTabletStockPage";
import NutritionWifaPage from "./pages/NutritionWifaPage";
import ReportsPage from "./pages/ReportsPage";
import StudentAddPage from "./pages/StudentAddPage";
import StudentListPage from "./pages/StudentListPage";
import TeacherAddPage from "./pages/TeacherAddPage";
import TeacherListPage from "./pages/TeacherListPage";
import ProtectedRoute from "./routes/ProtectedRoute";
function App() {
  return (
    <div className="App">
      <Routes>
			<Route path='/' element={<Home/>}/>
			{/* class */}
			{/* <ProtectedRoute></ProtectedRoute> */}
			<Route element={<ProtectedRoute/>}>
				<Route path='/teacher-list' element={<TeacherListPage/>}/>
				<Route path='/teacher-add' element={<TeacherAddPage/>}/>
				<Route path='/student-list' element={<StudentListPage/>}/>
				<Route path='/student-add' element={<StudentAddPage/>}/>
				<Route path='/class' element={<ClassListPage/>}/>
				{/* nutrition */}
				<Route path='/nutrition-education' element={<NutritionEducationPage/>}/>
				<Route path='/nutrition-wifa' element={<NutritionWifaPage/>}/>
				<Route path='/nutrition-deworming' element={<NutritionDewormingPage/>}/>
				<Route path='/nutrition-bmi' element={<NutritionBmiPage/>}/>
				<Route path='/nutrition-referral' element={<NutritionReferralPage/>}/>
				<Route path='/nutrition-table-stock' element={<NutritionTabletStockPage/>}/>
			</Route>
			{/* report  */}
			<Route path='/login' element={<LoginPage/>}/>
			<Route path='*' element={<NotFoundPage/>}/>
      </Routes>
    </div>
  );
}

export default App;
