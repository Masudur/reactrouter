import React, { Component } from 'react'

class MouseTracker extends Component {
    constructor(props){
        super(props);
        this.state = {x:0,y:0};
    }
    onMouseMoveHandler = (event)=>{
        this.setState({
            x:event.clientX,
            y:event.clientY
        })
    }
  render() {
    return (
      <div style={{height:'100vh',width:'100vw'}} onMouseMove={this.onMouseMoveHandler}>
        <div>mouse position</div>
        <div>Mouse position X: {this.state.x}</div>
        <div>Mouse position Y: {this.state.y}</div>
        {this.props.render(this.state)}
      </div>
    )
  }
}
export default MouseTracker;