import React, { Component } from 'react'
import MouseTracker from './MouseTracker';

class Dog extends Component {
  render() {
    return (
        <MouseTracker 
            render={(mouse)=>{
                return <div style={{position:'absolute',top:mouse.y,left:mouse.x}}>
                    <h1>Dog</h1>
                </div>
        }}/>
    )
  }
}
export default Dog;