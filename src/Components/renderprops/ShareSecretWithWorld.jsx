import React from 'react'
import ShareSecretToLife from './ShareSecretToLife'

const ShareSecretWithWorld = () => {
  return (
    <div>
        <ShareSecretToLife render={({secretToLife} )=>{
            return(
                <div>
                    <h1>this is render text {secretToLife}</h1>
                </div>
            )
        }}/>
    </div>
  )
}

export default ShareSecretWithWorld