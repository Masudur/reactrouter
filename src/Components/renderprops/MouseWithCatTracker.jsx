import React, { Component } from 'react'
import Cat from './Cat';

class MouseWithCatTracker extends Component {
    constructor(props){
        super(props);
        this.state = {x:0,y:0};
    }
    onMouseMoveHandler = (event)=>{
        this.setState({
            x:event.clientX,
            y:event.clientY
        })
    }
  render() {
    return (
      <div style={{height:'100vh',width:'100vw'}} onMouseMove={this.onMouseMoveHandler}>
        <div>mouse</div>
        <div>Mouse position X: {this.state.x}</div>
        <div>Mouse position Y: {this.state.y}</div>
        <Cat mouse={this.state}/>
      </div>
    )
  }
}
export default MouseWithCatTracker;