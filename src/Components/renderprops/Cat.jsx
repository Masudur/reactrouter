import React, { Component } from 'react'
import MouseTracker from './MouseTracker';

class Cat extends Component {
  render() {
    return (
        <MouseTracker 
            render={(mouse)=>{
                return <div style={{position:'absolute',top:mouse.y+50,left:mouse.x}}>
                    <h1>cat</h1>
                </div>
        }}/>
    )
  }
}
export default Cat;