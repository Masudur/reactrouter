import React from 'react'
import ShareSecretToLife from './ShareSecretToLife'

const Masud = () => {
  return (
    <div>
        <ShareSecretToLife render={({secretToLife})=>{
            return(
                <h3>My age is {secretToLife}</h3>
            )
        }}/>
    </div>
  )
}

export default Masud