import React, { Component } from 'react'
import Emoji from './Emoji'

export default class TextChild extends Emoji {
  render() {
    const decoratedText = this.addEmoji(' --- I love zaya --- ', "love")
    console.log("🚀 ~ file: TextChild.jsx ~ line 6 ~ TextChild ~ render ~ decoratedText", decoratedText)
    return super.render(decoratedText)
  }
}
