import React, { Component } from 'react'

class Clock extends Component {
    render() {
      console.log('--------------------',this.props)
    return (
      <div>
        <h1>Clock</h1>
        <h1>{new Date().toLocaleString(this.props.local)}</h1>
      </div>
    )
  }
}
export default Clock;