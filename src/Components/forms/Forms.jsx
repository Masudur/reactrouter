import React, { Component } from 'react'

export default class Forms extends Component {
    constructor(props){
        super(props);
        this.state = {
            isGoing: true,
            numberOfGuests: 2
        }
        this.handleInputChange = this.handleInputChange.bind(this)
    }
    handleInputChange(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }
  render() {
    const {isGoing,numberOfGuests} = this.state;
   
    return (
      <div className='container'>
        <div className="card mt-5">
            <div className="card-body">
            <form>
                <label>
                Is going:
                <input
                    name="isGoing"
                    type="checkbox"
                    checked={isGoing}
                    onChange={this.handleInputChange} />
                </label>
                <br />
                <label>
                Number of guests:
                <input
                    name="numberOfGuests"
                    type="number"
                    value={numberOfGuests}
                    onChange={this.handleInputChange} />
                </label>
            </form>
            </div>
        </div>
      </div>
    )
  }
}
