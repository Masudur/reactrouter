import React, { Component } from 'react'
import BoilingVerdict from './BoilingVerdict';
import TemperatureInput from './TemperatureInput';

class Calculator extends Component {
    
  render() {
    
    
    return (
      <div className='container'>
        <div className="card">
            <div className="card-body">
                <TemperatureInput scale="c" />
                <TemperatureInput scale="f" />
                <BoilingVerdict celsius={parseFloat(100)} />
            </div>
        </div>
      </div>
    )
  }
}

export default Calculator