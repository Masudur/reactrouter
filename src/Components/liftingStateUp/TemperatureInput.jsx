import React, { Component } from 'react'
const scaleNames = {
    c: 'Celsius',
    f: 'Fahrenheit'
  };

export default class TemperatureInput extends Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {temperature: ''};
    }
    handleChange(event){
        this.setState({temperature: event.target.value})
    }
  render() {
    const temperature = this.state.temperature;
    console.log("🚀 ~ file: TemperatureInput.jsx ~ line 18 ~ TemperatureInput ~ render ~ temperature", temperature)
    const scale = this.props.scale
    console.log("🚀 ~ file: TemperatureInput.jsx ~ line 19 ~ TemperatureInput ~ render ~ scale", scale)
    return (
        <div className='container'>
        <div className="card">
            <div className="card-body">
            <fieldset>
            <legend>Enter temperature in {scaleNames[scale]}:</legend>
                    <input
                        value={temperature}
                        onChange={this.handleChange} />
                   
            </fieldset>
            </div>
        </div>
      </div>
    )
  }
}
