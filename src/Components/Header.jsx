import React from 'react'
import { Link, NavLink } from "react-router-dom";
const Header = () => {
  return (
    <React.Fragment>
        <nav className="navbar navbar-expand-lg bg-light">
  <div className="container-fluid">
    <a className="navbar-brand" href="/">Navbar</a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="/navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
              <a class="nav-link" href="/">
                home
              </a>
        </li>
        
		<li className="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="programs" id="class-room" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              class room
          </a>
            <ul class="dropdown-menu" aria-labelledby="class-room">
            	  <li><Link class="dropdown-item" to="/teacher-add">teacher add</Link></li>
              	<li><Link class="dropdown-item" to="/teacher-list">teacher list</Link></li>
              	<li><Link class="dropdown-item" to="/student-list">student list</Link></li>
              	<li><Link class="dropdown-item" to="/student-add">student add</Link></li>
              	<li><Link class="dropdown-item" to="/class">class list </Link></li>
            </ul>
        </li>
        {/* nutrition  */}
        <li className="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="programs" id="nutrition-programs" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              nutrition programs
          </a>
            <ul class="dropdown-menu" aria-labelledby="nutrition-programs">
              <li><Link class="dropdown-item" to="/nutrition-education">nutrition education</Link></li>
              <li><Link class="dropdown-item" to="/nutrition-wifa">nutrition wifa</Link></li>
              <li><Link class="dropdown-item" to="/nutrition-deworming">nutrition deworming</Link></li>
              <li><Link class="dropdown-item" to="/nutrition-bmi">nutrition bmi</Link></li>
              <li><Link class="dropdown-item" to="/nutrition-referral">nutrition referral</Link></li>
              <li><Link class="dropdown-item" to="/nutrition-table-stock">nutrition table-stock</Link></li>
            </ul>
        </li>

      </ul>
      <div className="d-flex" role="search">
        <ul className='navbar-nav'>
          <li className="nav-item"> 
            <NavLink to='/' className="nav-link">user profile</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to='/login'  className="nav-link ">login</NavLink>
          </li>
        </ul>
       
      </div>
    </div>
  </div>
</nav>
    </React.Fragment>
  )
}

export default Header