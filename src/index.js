import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";
import Welcome from './Components/Welcome';
import Clock from './Components/Clock';
import Forms from './Components/forms/Forms';
import Calculator from './Components/liftingStateUp/Calculator';
import Emoji from './Components/inheritence/Emoji';
import TextChild from './Components/inheritence/TextChild';
import MouseTracker from './Components/renderprops/MouseTracker';
import MouseWithCatTracker from './Components/renderprops/MouseWithCatTracker';
import Cat from './Components/renderprops/Cat';
import Dog from './Components/renderprops/Dog';
import ShareSecretWithWorld from './Components/renderprops/ShareSecretWithWorld';
import Masud from './Components/renderprops/Masud';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      {/* <App /> */}
      {/* <Clock local="bn-BD"/> */}
      {/* <Forms/> */}
      {/* lifting state up  */}
      {/* <Calculator /> */}

      {/* composition inheritance */}
      {/* <Emoji/> */}
      {/* <TextChild/> */}
      {/* render props  */}
      {/* <MouseTracker/> */}
      {/* <Cat/> */}
      {/* <ShareSecretWithWorld/> */}
      {/* <Masud/> */}
      
      <hr />
      {/* <Dog /> */}
      {/* <MouseWithCatTracker/> */}
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
